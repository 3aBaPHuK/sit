import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import store from './store'

Vue.use(VueRouter)
Vue.use(Vuex)

import UkDispatch from './views/uk-dispatch/UkDispatch.vue'
import SntDispatch from './views/snt-dispatch/SntDispatch.vue'
import Software from './views/software/Software.vue'
import Products from './views/Products.vue'
import Category from './views/Category.vue'
import Product from './views/product/Product.vue'
import Services from './views/Services.vue'
import Cases from './views/Cases.vue'
import Case from './views/case/Case.vue'
import Support from './views/Support.vue'
import Contacts from './views/Contacts.vue'
import Main from './views/main/Main.vue'

const routes = [
    {path: '/', component: Main},
    {path: '/products/', component: Products},
    {path: '/products/:category/:id', component: Product},
    {path: '/products/dispatching-snt', component: SntDispatch},
    {path: '/products/uk-dispatch', component: UkDispatch},
    {path: '/products/software', component: Software},
    {path: '/products/:category', component: Category},
    {path: '/services', component: Services},
    {path: '/cases', component: Cases},
    {path: '/cases/:id', component: Case},
    {path: '/support', component: Support},
    {path: '/contacts', component: Contacts}
]

const router = new VueRouter({
    routes
});

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router: router,
    mutations: {
        setRouteActive: (state) => {
            console.log(state)
        }
    }
});
