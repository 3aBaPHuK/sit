const state = {
    routes: [
        {path: '/products/water', name: 'Продукты', subs: true},
        {path: '/services', name: 'Услуги'},
        {path: '/cases', name: 'Наши кейсы'},
        {path: '/support', name: 'Поддержка'},
        {path: '/contacts', name: 'Контакты'}
    ],
}

export default {
    state
}