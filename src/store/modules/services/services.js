const state = {
    services: [
        {
            type: 'left',
            icon: 'monitoring.svg',
            background: '#005895',
            bigHeading: 'Разработка<br/>специализированного<br/>ПО',
            buttonText: 'Получить услугу',
            buttonClass: 'yellow big no-float',
            descHeading: 'Описание услуги',
            description: 'Счетчик электронный крыльчатый применяется для учета расхода холодной и горячей воды с накоплением и передачей этих данных в сеть LaRoWAN',
            itemMarker: 'lines',
            itemMarkerColor: '#005895',
            caseLink: true,
            items: [
                'Определение проблемы',
                'Разработка путей решения',
                'Утверждение решения и планов работы',
                'Реализация решения',
                'Техническая поддержка и контроль'
            ]
        },
        {
            type: 'right',
            icon: 'instructions.svg',
            background: '#3aad3a',
            bigHeading: 'Разработка уникальных<br/>технических<br/>решений',
            buttonText: 'Получить услугу',
            buttonClass: 'white big no-float',
            descHeading: 'Как мы работаем',
            description: 'Счетчик электронный крыльчатый применяется для учета расхода холодной и горячей воды с накоплением и передачей этих данных в сеть LaRoWAN',
            itemMarker: 'numbers',
            itemMarkerColor: '#3aad3a',
            caseLink: true,
            items: [
                'Определение проблемы',
                'Разработка путей решения',
                'Утверждение решения и планов работы',
                'Реализация решения',
                'Техническая поддержка и контроль'
            ]
        },
        {
            type: 'left',
            icon: 'network-connection.svg',
            background: '#0b315d',
            bigHeading: 'Построение<br/>сети',
            buttonText: 'Получить услугу',
            buttonClass: 'yellow big no-float',
            descHeading: 'Как строим сеть',
            description: 'Счетчик электронный крыльчатый применяется для учета расхода холодной и горячей воды с накоплением и передачей этих данных в сеть LaRoWAN',
            itemMarker: 'numbers',
            itemMarkerColor: '#0b315d',
            caseLink: true,
            items: [
                'Определение проблемы',
                'Разработка путей решения',
                'Утверждение решения и планов работы',
                'Реализация решения',
                'Техническая поддержка и контроль'
            ]
        },
        {
            type: 'right',
            icon: 'presentation (2).svg',
            background: '#ea4818',
            bigHeading: 'Консалтинговые<br/>услуги',
            buttonText: 'Получить услугу',
            buttonClass: 'white big no-float',
            descHeading: 'Мы специалисты в области',
            description: 'Счетчик электронный крыльчатый применяется для учета расхода холодной и горячей воды с накоплением и передачей этих данных в сеть LaRoWAN',
            itemMarker: 'lines',
            itemMarkerColor: '#ea4818',
            caseLink: true,
            items: [
                'Определение проблемы',
                'Разработка путей решения',
                'Утверждение решения и планов работы',
                'Реализация решения',
                'Техническая поддержка и контроль'
            ]
        }
    ]
}

export default {
    state
}