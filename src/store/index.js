import Vue from 'vue'
import Vuex from 'vuex'
import products from './modules/products/products.js'
import menu from './modules/menu/menu.js'
import data from './modules/data/data.js'
import services from './modules/services/services.js'
import cases from './modules/cases/cases'
import faq from './modules/faq/faq.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        products,
        menu,
        data,
        services,
        cases,
        faq
    },
    getters: {
        getPopularProducts: state => {
            let populars = {};

            Object.keys(state.products).map((objectKey, index) => {
                let products = state.products[objectKey];
                Object.keys(products).map((productKey, productIndex) => {
                    let product = products[productKey];
                    product.category = objectKey;

                    if (product.popular) {
                        populars[productKey] = product;
                    }
                });
            });

            return populars;
        },
        getCaseById: state => id => {
            return state.cases.cases.find(todo => todo.id === id);
        }
    },
    mutations: {
        toggleQuestion(state, question) {
            question.collapsed = !question.collapsed
        },
        filterQuestions(state, value) {
            let questions = [];

            state.faq.left.forEach(section => {
                section.questions.forEach(question => {
                    questions.push(question)
                })
            })

            state.faq.right.forEach(section => {
                section.questions.forEach(question => {
                    questions.push(question)
                })
            })

            questions.forEach(function (q) {
                q.hidden = true
            })

            let res = questions.filter(
                question => {
                    return question.heading.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
                    question.desc.toLowerCase().indexOf(value.toLowerCase()) !== -1});

            res.forEach(function (elem) {
                elem.hidden = false;
            })
        }

    }
})